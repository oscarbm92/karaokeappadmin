import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { mergeMap } from 'rxjs/operators';

@Injectable()
export class YoutubeProvider {

  apiKey = 'AIzaSyAyUosE5S05El0L59uO59mVcxLEXj2cgHk';
  maxresults = '10';
  part = 'snippet';
  videoSyndicated= 'true';
  videoEmbeddable='true';
  type = 'video';
  //topicId='karaoke';
  //videoCategoryId1='10';
  //videoCategoryId2='24';
  regionCode = 'MX';
  videoDefinition = 'high';

  constructor(public http: HTTP) {
    
  }

  getYoutubeSearch(ev: any){
    let val = ev.target.value + ' karaoke';
    let query = 'https://www.googleapis.com/youtube/v3/search?key='+
    this.apiKey+
    '&q='+val+
    '&part='+this.part+
    '&maxResults='+this.maxresults+
    '&videoSyndicated='+this.videoSyndicated+
    '&videoEmbeddable='+this.videoEmbeddable+
    '&type='+this.type+
    //'&topicId='+this.topicId+
    '&regionCode='+this.regionCode;
    //'&videoDefinition='+this.videoDefinition;
    
    /*return this.http.get(query+this.videoCategoryId1).pipe(
      mergeMap(character => this.http.get(query+this.videoCategoryId2))
    ).map((res) => {
      return res['items'];
    })*/

    return this.http.get(query, {}, {});
  }

}
