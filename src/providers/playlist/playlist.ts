import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the PlaylistProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PlaylistProvider {

  public headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Basic a2FyYW9rZWFkbWluOnF3ZXJ0eTEyMzQ1Ng=='
  };

  constructor(public http: HTTP) {

  }

  getPlayList(){
    return this.http.get('http://berreokaraokebar.com/api/?action=getPlaylistWithUserInfo', {}, this.headers);
  }

  setAltaPrioridad(id){
    this.http.setDataSerializer('json');
    return this.http.patch('http://berreokaraokebar.com/crm/api/v1/listaDeReproduccion/'+id, {
      'altaprioridad': true,
      'turnoAltaPrioridad': '2'
    }, this.headers);
  }

  orderPlaylist(){
    return this.http.get('http://berreokaraokebar.com/api/?action=orderplaylist', {}, this.headers);
  }

  getAltaPrioridad(){
    return this.http.get('http://berreokaraokebar.com/crm/api/v1/listaDeReproduccion?where[0][type]=equals&where[0][field]=altaprioridad&where[0][value]=1', {}, this.headers);
  }

  sendPushAntaPrioridad(song){
    this.http.setDataSerializer('json');
    //console.log(songObj);
    return this.http.post('https://fcm.googleapis.com/fcm/send', {
      "to" : song.fbtoken,
      "collapse_key" : "type_a",
      "data" : {
        "body" : "Tu canción: "+song.name+" ha sido marcada con prioridad alta. ¡Se reproducirá en breve!",
        "title": "¡Eres alta prioridad!",
        "key_1" : "Data for key one",
        "key_2" : "Hellowww"
      }
    }, {
      "Authorization": "key=AAAASYyu0Io:APA91bFzBxe8q3iIwyVKGDxDWepJPe7TNVqUWc5A_EmmxidiJrtbI0UBIwjdpuoh5fMGtm6UeyhC5oUSGgOKB4UwMEl6jk15ghMFpaKOYODDLrCFioexH52vGduMVcuiGPkCUnp2d9zn",
      "Content-Type": "application/json",
    });
  }
  

}
