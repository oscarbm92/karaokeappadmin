import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';
import { MesasPage } from '../../pages/mesas/mesas';


@Injectable()
export class ControlProvider {

  public user = '';

  // Headers
  private headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Basic a2FyYW9rZWFkbWluOnF3ZXJ0eTEyMzQ1Ng=='
  };

  private baseURLApiCrm = 'http://berreokaraokebar.com/crm/api/v1/';
  private baseURLCustomApi = 'http://berreokaraokebar.com/api/';

  constructor(private http: HTTP) {
  }

  getMesas(){
    return this.http.get(this.baseURLApiCrm + 'Mesas', {}, this.headers);
  }

  desactivateUser(userid){
    this.http.setDataSerializer('json');
    return this.http.put(this.baseURLApiCrm + 'Account/'+userid, {
      'activo': false,
      'mesaName' : '',
      'mesaId' : '',
      'assignedUserId': ''
    }, this.headers);
  }

  getSongsByTable(mesaId){
    return this.http.get(this.baseURLApiCrm + 'ListaDeReproduccion?where[0][type]=equals&where[0][field]=mesaId&where[0][value]='+mesaId, {}, this.headers);
  }

  getClientsByTable(mesaId){
    return this.http.get(this.baseURLApiCrm + 'Account?where[0][type]=equals&where[0][field]=mesaId&where[0][value]='+mesaId, {}, this.headers);
  }

  desactivateTable(mesaId){
    return this.http.delete(this.baseURLApiCrm + 'Mesas/'+mesaId, {}, this.headers);
  }

  deleteSong(songId){
    return this.http.delete(this.baseURLApiCrm + 'ListaDeReproduccion/'+songId, {}, this.headers);
  }

  deleteVotes(songId){
    return this.http.delete(this.baseURLApiCrm + 'votos/'+songId, {}, this.headers);
  }

  NewMesa(mesaName, user, description, maximoCanciones) {
    this.http.setDataSerializer('json');
    return this.http.post(this.baseURLApiCrm + 'Mesas', {
      'name': mesaName,
      'description': description,
      'assignedUserId': user,
      'songsLimit': maximoCanciones
    }, this.headers);
  }

  login(token, username){
    let headerslogin = { 
      'Content-Type': 'application/json',
      'Authorization': token
    }
    return this.http.get(this.baseURLApiCrm + 'User?where[0][type]=equals&where[0][field]=userName&where[0][value]='+username, {}, headerslogin);
  }

  setCuentaAppCreada(user) {    
    this.http.setDataSerializer('json');
    return this.http.put(this.baseURLApiCrm + 'User/'+user, {
      'cuentaDeAppCreada' : true
    }, this.headers);
  }

  getNameMesa(mesaName){
    return this.http.get(this.baseURLApiCrm + 'Mesas?where[0][type]=equals&where[0][field]=name&where[0][value]='+mesaName, {}, this.headers);
  }

  getUserIsActive(uid){
    return this.http.get(this.baseURLApiCrm + 'User/'+uid, {}, this.headers);
  }

  setUserFirebaseToken(uid, token){
    this.http.setDataSerializer('json');
    return this.http.patch(this.baseURLApiCrm + 'User/'+uid, {
      'fbtoken': token
    }, this.headers);
  }

  getUsers(){
    return this.http.get(this.baseURLCustomApi + '?action=getUserData', {}, this.headers);
  }

  changeUserStatus(id, status){
    this.http.setDataSerializer('json');
    return this.http.patch(this.baseURLApiCrm + 'User/'+id, {
      'isActive': status,
    }, this.headers);
  }

  getTeams(){
    return this.http.get(this.baseURLApiCrm + 'Team', {}, this.headers);
  }

  getRoles(){
    return this.http.get(this.baseURLApiCrm + 'Role', {}, this.headers);
  }
  
  setUserWithPassword(user){
    this.http.setDataSerializer('json');
    return this.http.patch(this.baseURLApiCrm + 'User/'+user.userId, {
      'esAdmin' : user.esAdmin,
      'firstName' : user.firstName,
      'lastName' : user.lastName,
      'password' : user.password,
      'userName' : user.userName
    }, this.headers);
  }

  setUser(user){
    this.http.setDataSerializer('json');
    return this.http.patch(this.baseURLApiCrm + 'User/'+user.userId, {
      'esAdmin' : user.esAdmin,
      'firstName' : user.firstName,
      'lastName' : user.lastName,
      'userName' : user.userName
    }, this.headers);
  }

  setTeamAndRole(user){
    return this.http.get(this.baseURLCustomApi + '?action=updateUser&userId='+user.userId+'&tuid='+user.teamUserId+'&ruid='+user.roleUserId+'&tid='+user.teamId+'&rid='+user.roleId, {}, this.headers);
  }


  createUser(user){
    this.http.setDataSerializer('json');
    return this.http.post(this.baseURLApiCrm + 'User', {
      "name": user.firstName + user.lastName,
      "isAdmin": false,
      "userName": user.userName,
      "firstName": user.firstName,
      "lastName": user.lastName,
      "isActive": true,
      "emailAddress": user.emailAddress,
      "cuentaDeAppCreada": false,
      "esAdmin": user.esAdmin,
      "password": user.password,
      "defaultTeamId": user.teamId,
      "defaultTeamName": user.team
    }, this.headers);
  }

  createTeamAndRole(user, newUserId){
    let s = this.baseURLCustomApi + '?action=createUser&userId='+newUserId+'&tid='+user.teamId+'&rid='+user.roleId;
    console.log(s);
    return this.http.get(s, {}, this.headers);
  }

  deleteUser(id){
    return this.http.delete(this.baseURLApiCrm + 'User/'+id, {}, this.headers);
  }

  editMesa(mesa){
    this.http.setDataSerializer('json');
    return this.http.put(this.baseURLApiCrm + 'Mesas/'+mesa.id, {
      'description': mesa.description,
      'songsLimit' : mesa.songsLimit
    }, this.headers);
  }

}
