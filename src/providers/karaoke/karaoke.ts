import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the KaraokeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class KaraokeProvider {

  public headers = {
    'Content-Type': 'application/json',
    'Authorization': 'Basic a2FyYW9rZWFkbWluOnF3ZXJ0eTEyMzQ1Ng=='
  };

  constructor(public http: HTTP) {
    
  }

  getMesa(qrcode){
    return this.http.get('http://berreokaraokebar.com/crm/api/v1/Mesas/'+qrcode, {}, this.headers);
  }

  activateUser(user, mesainfo){
    this.http.setDataSerializer('json');
    return this.http.patch('http://berreokaraokebar.com/crm/api/v1/Account/'+user.id, {
      'activo': true,
      'mesaId': mesainfo.id,
      'mesaName': mesainfo.name,
      'visitas': user.visitas+1,
      'assignedUserId': mesainfo.assignedUserId
    }, this.headers);
  }

  activateMesa(mesainfo){
    this.http.setDataSerializer('json');
    return this.http.patch('http://berreokaraokebar.com/crm/api/v1/Mesas/'+mesainfo.id, {
      'ocupada': true,
      'turnoRonda': mesainfo.turnoRonda
    }, this.headers);
  }

  addToPlaylist(songObj){
    this.http.setDataSerializer('json');
    //console.log(songObj);
    return this.http.post('http://berreokaraokebar.com/crm/api/v1/ListaDeReproduccion', {
      'name': songObj.snippet.title,
      'idYoutube': songObj.id.videoId,
      'mesaName': songObj.mesa.name,
      'mesaId': songObj.mesa.id,
      'accountId': songObj.user.id,
      'accountName': songObj.user.name,
      'calificable': songObj.calificable,
      'assignedUserId': songObj.assignedUserId
    }, this.headers);
  }

  GetNumSongsByTable(mesa){
    return this.http.get('http://berreokaraokebar.com/crm/api/v1/listaDeReproduccion?where[0][type]=equals&where[0][field]=mesaId&where[0][value]='+mesa+'&where[1][type]=equals&where[1][field]=played&where[1][value]=false', {}, this.headers);
  }

  getPlayList(){
    //return this.http.get('http://berreokaraokebar.com/crm/api/v1/listaDeReproduccion?where[0][type]=equals&where[0][field]=played&where[0][value]=0&sortBy=turno&asc=true', {}, this.headers);
    return this.http.get('http://berreokaraokebar.com/api/?action=getPlaylistWithUserInfo', {}, this.headers);
  }

  getPlayingSong(){
    return this.http.get('http://berreokaraokebar.com/crm/api/v1/listaDeReproduccion?where[0][type]=equals&where[0][field]=playing&where[0][value]=1', {}, this.headers);
  }

  getTurnoRonda(){
    return this.http.get('http://berreokaraokebar.com/crm/api/v1/Mesas?sortBy=turnoRonda&asc=false&maxSize=1', {}, this.headers);
  }

  /*setTurnoRonda(mesa){
    return this.http.put('http://berreokaraokebar.com/crm/api/v1/Mesas/'+mesa.id, {
      'turnoRonda': mesa.turnoRonda
    }, this.headers);
  }*/

  orderPlaylist(){
    return this.http.get('http://berreokaraokebar.com/api/?action=orderplaylist', {}, this.headers);
  }

  rateSong(objSong){
    this.http.setDataSerializer('json');
    return this.http.put('http://berreokaraokebar.com/crm/api/v1/listaDeReproduccion/'+objSong.id, {
      'likes': objSong.likes,
      'dislikes': objSong.dislikes
    }, this.headers);
  }

  deleteSong(song){
    return this.http.delete('http://berreokaraokebar.com/crm/api/v1/listaDeReproduccion/'+song.id, {}, this.headers);
  }

  editSong(newSong, oldSong){
    this.http.setDataSerializer('json');
    return this.http.patch('http://berreokaraokebar.com/crm/api/v1/listaDeReproduccion/'+oldSong.id, {
      'name': newSong.snippet.title,
      'idYoutube': newSong.id.videoId
    }, this.headers);
  }

  getPodio(){
    return this.http.get('http://berreokaraokebar.com/api/?action=getPodio', {}, this.headers);
  }

  setVotoLike(song, user){
    this.http.setDataSerializer('json');
    return this.http.post('http://berreokaraokebar.com/crm/api/v1/votos', {
      'tipo': 'like',
      'clienteId': user.id,
      'clienteName': user.name,
      'cancionId': song.id,
      'cancionName': song.name
    }, this.headers);
  }

  setVoto(user, tipo){
    this.http.setDataSerializer('json');
    //console.log(songObj);
    return this.http.post('http://berreokaraokebar.com/crm/api/v1/votos', {
      'tipo': tipo,
      'clienteId': user.id,
      'clienteName': user.name,
    }, this.headers);
  }

  getUserVoteSong(user, song){
    return this.http.get('http://berreokaraokebar.com/crm/api/v1/votos?where[0][type]=equals&where[0][field]=clienteId&where[0][value]='+user.id+'&where[1][type]=equals&where[1][field]=cancionId&where[1][value]='+song.id+'&where[2][type]=equals&where[2][field]=tipo&where[2][value]=like', {}, this.headers);
  }

  getUserVotePause(user){
    return this.http.get('http://berreokaraokebar.com/crm/api/v1/votos?where[0][type]=equals&where[0][field]=clienteId&where[0][value]='+user.id+'&where[2][type]=equals&where[2][field]=tipo&where[2][value]=pausa', {}, this.headers);
  }

  getUserVoteReanudar(user){
    return this.http.get('http://berreokaraokebar.com/crm/api/v1/votos?where[0][type]=equals&where[0][field]=clienteId&where[0][value]='+user.id+'&where[2][type]=equals&where[2][field]=tipo&where[2][value]=reanudar', {}, this.headers);
  }

  getTotalVotos(tipo){
    return this.http.get('http://berreokaraokebar.com/api/?action=calculateDjOrKaraoke&tipo='+tipo, {}, this.headers);
  }

  getSongsToSwitch(song){    
    return this.http.get('http://berreokaraokebar.com/crm/api/v1/listaDeReproduccion?where[0][type]=equals&where[0][field]=mesaId&where[0][value]='+song.mesaId+'&where[1][type]=after&where[1][field]=turno&where[1][value]='+song.turno+'&where[2][type]=equals&where[2][field]=played&where[2][value]=0', {}, this.headers);
  }

  switchSongs(s1, s2){    
    let q = 'http://berreokaraokebar.com/api/?action=switchSongs&s1='+s1+'&s2='+s2;
    console.log(q);
    
    return this.http.get(q, {}, this.headers);
  }

  getSingleSong(id){
    return this.http.get('http://berreokaraokebar.com/crm/api/v1/listaDeReproduccion/'+id, {}, this.headers);
  }

}
