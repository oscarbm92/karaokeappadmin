import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { Firebase } from '@ionic-native/firebase';
import firebase from 'firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;
  
  pages: Array<{title: string, component: any, icon: string, action: string}>;
  public user = {displayName:'', email:''};

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public firebase: Firebase, private cd: ChangeDetectorRef) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Cerrar sesión', component: LoginPage, icon: 'log-out', action: 'cerrarSesion' }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      if(this.platform.is('android')) {
        this.statusBar.styleBlackOpaque();
      }
      this.splashScreen.hide();
      let t = this;
      firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          t.user = user;
          t.cd.detectChanges();
        }
      });
      

    });
  }

  openPage(page) {
    switch(page.action) { 
      case 'cerrarSesion': { 
        let x = this;
        firebase.auth().signOut().then(function() {
          //x.menuCtrl.swipeEnable(false);
          x.nav.setRoot(LoginPage);
        }).catch(function(error) {
          // An error happened.
        });
        break; 
      }   
      default: { 
         //statements; 
         break; 
      } 
   }
    
  }
}
