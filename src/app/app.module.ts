import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ControlProvider } from '../providers/control/control';
import { LoginPage } from '../pages/login/login';
import firebase  from 'firebase';
import { Firebase } from '@ionic-native/firebase';

import { HTTP } from '@ionic-native/http';

import { IonicStorageModule } from '@ionic/storage';
import { NewTablePage } from '../pages/new-table/new-table';

import { IonicImageViewerModule } from 'ionic-img-viewer';
import { MesasPage } from '../pages/mesas/mesas';
import { PlaylistPage } from '../pages/playlist/playlist';
import { SomethingPage } from '../pages/something/something';
import { PlaylistProvider } from '../providers/playlist/playlist';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { UserAdminPage } from '../pages/user-admin/user-admin';
import { NewUserPage } from '../pages/new-user/new-user';
import { EditUserPage } from '../pages/edit-user/edit-user';
import { EditSongPage } from '../pages/edit-song/edit-song';
import { YoutubeProvider } from '../providers/youtube/youtube';
import { KaraokeProvider } from '../providers/karaoke/karaoke';
import { TableDetailPage } from '../pages/table-detail/table-detail';

firebase.initializeApp({
  apiKey: "AIzaSyAd9xm85kEZ7aD54W2h7VC-HxRPlxImHO4",
  authDomain: "karaokeadmin.firebaseapp.com",
  databaseURL: "https://karaokeadmin.firebaseio.com",
  projectId: "karaokeadmin",
  storageBucket: "karaokeadmin.appspot.com",
  messagingSenderId: "140652007822"
})


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    NewTablePage,
    MesasPage,
    PlaylistPage,
    SomethingPage,
    UserAdminPage,
    NewUserPage,
    EditUserPage,
    EditSongPage,
    TableDetailPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    IonicImageViewerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    NewTablePage,
    MesasPage,
    PlaylistPage,
    SomethingPage,
    UserAdminPage,
    NewUserPage,
    EditUserPage,
    EditSongPage,
    TableDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ControlProvider,
    HTTP,
    PlaylistProvider,
    Firebase,
    YoutubeVideoPlayer,
    YoutubeProvider,
    KaraokeProvider
  ]
})
export class AppModule {}
