import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ControlProvider } from '../../providers/control/control';

@IonicPage()
@Component({
  selector: 'page-new-user',
  templateUrl: 'new-user.html',
})
export class NewUserPage {

  public user = {userId:'', firstName:'', lastName:'', userName:'', teamId:'', roleId: '', team:'', role:'', esAdmin:0, roleUserId: '', teamUserId: '', password:'', emailAddress:''}
  public newUserForm: FormGroup;
  public equipos = [];
  public roles = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, private cd: ChangeDetectorRef, private controlprovider: ControlProvider, public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
    this.createUserForm();
  }

  ionViewDidLoad() {
    this.getTeams();
  }

  public createUserForm(){
    //console.log(this.user);
    this.newUserForm = this.formBuilder.group({
      userName: ['', Validators.required],
      userLastName: ['', Validators.required],
      emailAddress: ['', Validators.required],
      user: ['', Validators.required],
      password: ['', Validators.minLength(6)], 
      equipos: ['', Validators.required],
      roles: ['', Validators.required],
      esAdmin: [false, Validators.required],
    });
  }

  createUser(){
    let loadinguuwp = this.loadingCtrl.create({
      content: 'Creando usuario...'
    });
    loadinguuwp.present();
    this.user.firstName = this.newUserForm.value.userName;
    this.user.lastName = this.newUserForm.value.userLastName;
    this.user.password = this.newUserForm.value.password;
    this.user.emailAddress = this.newUserForm.value.emailAddress;
    this.user.esAdmin = this.newUserForm.value.esAdmin;
    this.user.roleId = this.newUserForm.value.roles;
    this.user.teamId = this.newUserForm.value.equipos;
    this.user.userName = this.newUserForm.value.user;

    this.controlprovider.createUser(this.user).then(response =>{ 
      let data = JSON.parse(response.data);
      this.controlprovider.createTeamAndRole(this.user, data.id).then(response =>{
         
        loadinguuwp.dismiss();
        this.showSaved();
      },
      (error) =>{
        loadinguuwp.dismiss();
        console.log('Error updateUserWP-> '+JSON.stringify(error));
      });
    },
    (error) =>{
      loadinguuwp.dismiss();
      console.log('Error updateUserWP-> '+JSON.stringify(error));
    });
  }
  /*editUser(){
    this.user.firstName = this.editUserForm.value.userName;
    this.user.lastName = this.editUserForm.value.userLastName;
    this.user.userName = this.editUserForm.value.user;
    this.user.teamId = this.editUserForm.value.equipos;
    this.user.roleId = this.editUserForm.value.roles;
    this.user.esAdmin = this.editUserForm.value.esAdmin;
    this.user.password = this.editUserForm.value.password;

    if(this.editUserForm.value.password != ''){
      this.updateUserWithPassword();
    }else{
      this.updateUser();
    }
  }*/

  /*updateUserWithPassword(){
    let loadinguuwp = this.loadingCtrl.create({
      content: 'Modificando usuario...'
    });
    loadinguuwp.present();
    this.controlprovider.setUserWithPassword(this.user).then(response =>{ 
      let data = JSON.parse(response.data);
      loadinguuwp.dismiss();
      this.updateTeamAndRole();
    },
    (error) =>{
      loadinguuwp.dismiss();
      console.log('Error updateUserWP-> '+JSON.stringify(error));
    });
  }*/

  /*updateUser(){
    let loadinguuwp = this.loadingCtrl.create({
      content: 'Modificando usuario...'
    });
    loadinguuwp.present();
    this.controlprovider.setUser(this.user).then(response =>{ 
      let data = JSON.parse(response.data);
      loadinguuwp.dismiss();
      this.updateTeamAndRole();
    },
    (error) =>{
      loadinguuwp.dismiss();
      console.log('Error updateUser-> '+JSON.stringify(error));
    });
  }*/
  
  /*updateTeamAndRole(){
    //console.log(this.user);
    
    let loadingutr = this.loadingCtrl.create({
      content: 'Modificando Equipo y Rol...'
    });
    loadingutr.present();
    this.controlprovider.setTeamAndRole(this.user).then(response =>{ 
      //console.log(response);
      loadingutr.dismiss();
      this.showSaved();
    },
    (error) =>{
      console.log('Error updateteamAndRole-> '+JSON.stringify(error));
    });
  }*/

  showSaved(){    
    const confirm = this.alertCtrl.create({
      title: '¡Creado!',
      message: 'El usuario ha sido creado exitosamente.',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    confirm.present();
  }

  getTeams(){
    this.controlprovider.getTeams().then(response =>{ 
      let data = JSON.parse(response.data);
      this.equipos = data['list'];
      this.getRoles();
    },
    (error) =>{
      console.log('Error getTeams-> '+JSON.stringify(error));
    });
  } 

  getRoles(){
    this.controlprovider.getRoles().then(response =>{ 
      let data = JSON.parse(response.data);
      this.roles = data['list'];
      this.cd.detectChanges();
    },
    (error) =>{
      console.log('Error getRoles-> '+JSON.stringify(error));
    });
  }

}
