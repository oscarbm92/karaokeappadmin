import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, AlertController} from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import QRCode from 'qrcode';
import { Storage } from '@ionic/storage';
import { ControlProvider } from '../../providers/control/control';
import { HomePage } from '../home/home';

//@IonicPage()
@Component({
  selector: 'page-new-table',
  templateUrl: 'new-table.html',
})
export class NewTablePage {

  public mesaData = {"id": "", "name": ""};
  public user = '';
  public generated = '';
  public newTableForm: FormGroup;
  public formVisible:boolean = true;
  public loadingVisible:boolean = false;

  constructor(public navCtrl: NavController, private cd: ChangeDetectorRef, private storage: Storage, 
    private formBuilder: FormBuilder, private controlprovider: ControlProvider, public alertCtrl: AlertController) {
    this.createNewTableForm();
    this.storage.get('user').then((val) => {
      this.user = val.id;
    });

  }

  ionViewDidLoad() {
    
  }

  displayQrCode() {
    return this.generated !== '';
  }
  
  private createNewTableForm(){
    this.newTableForm = this.formBuilder.group({
      tableNumber: ['', Validators.required],
      maximoCanciones: [''],
      description: ['']
    });
  }

  process() {
    this.formVisible = false;
    this.loadingVisible = true;
    this.cd.detectChanges();

    this.controlprovider.getNameMesa(this.newTableForm.value.tableNumber).then((responseM) => {
      let dataM = JSON.parse(responseM.data);
      if(dataM['total'] == 0){
        this.controlprovider.NewMesa( this.newTableForm.value.tableNumber, this.user, this.newTableForm.value.description, this.newTableForm.value.maximoCanciones).then((responseNM) => {
          const t = this;
          this.mesaData = JSON.parse(responseNM.data);
          const qrcode = QRCode;
          qrcode.toDataURL(this.mesaData.id, { errorCorrectionLevel: 'H' }, function (err, url) {
            t.generated = url;
            t.loadingVisible = false;
            t.cd.detectChanges();
          })
        },
        (error) =>{
          console.log('Error newMesa-> '+JSON.stringify(error));
        });
      }else{
        this.formVisible = true;
        this.loadingVisible = false;
        this.cd.detectChanges();
        this.showMesaRepetida();      
      }
    },
    (error) =>{
      console.log('Error getNameMesa-> '+JSON.stringify(error));
    });

    
    
  }

  showMesaRepetida(){    
    const confirm = this.alertCtrl.create({
      title: 'ERROR',
      message: 'Ya existe ese número de mesa.',
      buttons: [{
        text: 'OK',
        handler: () => {
          
        }
      }]
    });
    confirm.present();
  }

  close(){
    this.navCtrl.setRoot(HomePage);
  }

  /*presentImage(myImage) {
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();
 
    setTimeout(() => imageViewer.dismiss(), 1000);
    imageViewer.onDidDismiss(() => alert('Viewer dismissed'));
  }*/
}
