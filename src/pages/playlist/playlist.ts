import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, ModalController, LoadingController} from 'ionic-angular';
import { PlaylistProvider } from '../../providers/playlist/playlist';
import { Storage } from '@ionic/storage';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { EditSongPage } from '../edit-song/edit-song';

/**
 * Generated class for the PlaylistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-playlist',
  templateUrl: 'playlist.html',
})
export class PlaylistPage {

  public playList = [];
  public user = {esAdmin:false, id:''};
  public mesa = null;
  public filtroPlaylist = 'todo';

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private playlistProvider: PlaylistProvider,
    public alertCtrl: AlertController, public modalCtrl: ModalController, public loadingCtrl: LoadingController, private cd: ChangeDetectorRef, private storage: Storage,
    private youtube: YoutubeVideoPlayer) {
    this.viewCtrl = this.navParams.get('viewCtrl');
  }

  /*ionViewDidLoad() {  
    let loadingPlaylist = this.loadingCtrl.create({
      content: 'Cargando...'
    });
    loadingPlaylist.present();

    this.storage.get('user').then((val) => {
      this.user = val;
    });

    this.playlistProvider.getPlayList().then(response =>{ 
      let data = JSON.parse(response.data);
      this.playList = data;
      //console.log(this.playList);
      this.onChangeFilter(this.filtroPlaylist);
      this.cd.detectChanges();
      loadingPlaylist.dismiss();
    },
    (error) =>{
      console.log('Error getPlayList-> '+JSON.stringify(error));
    });
  }*/

  previewVideo(idVideo){
    this.youtube.openVideo(idVideo);
  }
  
  ionViewDidEnter(){
    let loadingPlaylist = this.loadingCtrl.create({
      content: 'Cargando...'
    });
    loadingPlaylist.present();

    this.storage.get('user').then((val) => {
      this.user = val;
    });

    this.playlistProvider.getPlayList().then(response =>{ 
      let data = JSON.parse(response.data);
      this.playList = data;
      //console.log(this.playList);
      this.onChangeFilter(this.filtroPlaylist);
      this.cd.detectChanges();
      loadingPlaylist.dismiss();
    },
    (error) =>{
      console.log('Error getPlayList-> '+JSON.stringify(error));
    });
  }

  doRefresh(refresher) {
    let loadingPlaylist = this.loadingCtrl.create({
      content: 'Cargando...'
    });
    loadingPlaylist.present();
    this.playlistProvider.getPlayList().then(response =>{ 
      let data = JSON.parse(response.data);
      this.playList = data;
      //console.log(this.playList);
      
      this.onChangeFilter(this.filtroPlaylist);
      refresher.complete();
      this.cd.detectChanges();
      loadingPlaylist.dismiss();
    },
    (error) =>{
      console.log('Error getPlayList-> '+JSON.stringify(error));
    });
  }

  onChangeFilter(value){
    let t = this;
    if(value == 'misMesas'){
      Object.keys(this.playList).forEach(function(key,index) {  
        if(t.user.id == t.playList[index].assignedUserId){
          t.playList[index].show = true;
        }else{
          t.playList[index].show = false;
        }
      });
      this.cd.detectChanges();
    }else{
      Object.keys(this.playList).forEach(function(key,index) {
        t.playList[index].show = true;
      });
      this.cd.detectChanges();
    }
  }

  consultarAltaPrioridad(song){
    let loadingAltaPrioridad = this.loadingCtrl.create({
      content: 'Cargando...'
    });
    loadingAltaPrioridad.present();
    this.playlistProvider.getAltaPrioridad().then(response =>{ 
      let data = JSON.parse(response.data); 
      console.log(' get Alta Prioridad');
      console.log(data);
      
           
      loadingAltaPrioridad.dismiss();
      if(data['total'] == 0){
        this.showConfirmAltaPrioridad(song);
      }else{
        this.showNoAltaPrioridad();
      }
    },
    (error) =>{
      loadingAltaPrioridad.dismiss();
      console.log('Error getAltaPrioridad-> '+JSON.stringify(error));
    });
  }

  setAltaPrioridad(song){
    let loadingAltaPrioridad = this.loadingCtrl.create({
      content: 'Asignando Alta Prioridad...'
    });
    loadingAltaPrioridad.present();
    this.playlistProvider.setAltaPrioridad(song.id).then(response =>{ 
      let data = JSON.parse(response.data);
      this.playlistProvider.sendPushAntaPrioridad(song).then(response =>{ 
        let data = JSON.parse(response.data);
        this.playlistProvider.orderPlaylist().then(r =>{
          
        });
        loadingAltaPrioridad.dismiss();
      },
      (error) =>{
        loadingAltaPrioridad.dismiss();
        console.log('Error getAltaPrioridad-> '+JSON.stringify(error));
      });
    },
    (error) =>{
      loadingAltaPrioridad.dismiss();
      console.log('Error getAltaPrioridad-> '+JSON.stringify(error));
    });
  }

  showConfirmAltaPrioridad(song){
    const confirm = this.alertCtrl.create({
      title: 'Alta Prioridad',
      message: `<p>Seguro que quiere asignar Alta prioridad a:</p>
                <p>`+song.name+`</p>
                <p>De la mesa: `+song.mesaName+`</p>  
      `,
      buttons: [{
        text: 'No',
        handler: () => {
          
        }
      },
      {
        text: 'Si',
        handler: () => {
          this.setAltaPrioridad(song);
        }
      }
      ]
    });
    confirm.present();
  }

  showNoAltaPrioridad(){
    const confirm = this.alertCtrl.create({
      title: '¡Espera!',
      message: 'Ya existe una canción con alta prioridad, cuando se reproduzca podrás asignar alta prioridad a otra canción.',
      buttons: [{
        text: 'OK',
        handler: () => {
          
        }
      }]
    });
    confirm.present();
  }

  showEdit(songObj){    
    const confirm = this.alertCtrl.create({
      title: 'Editar Canción',
      message: `<p>Seguro/a que quieres Cambiar:</p>
                <p><strong>`+ songObj.name +`</strong> 
                <p>*Solo cambia la canción y tu turno se mantiene igual.</p>`,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            this.editSong(songObj);
          }
        }
      ]
    });
    confirm.present();
  }

  editSong(songObj){
    let editSongModal = this.modalCtrl.create(EditSongPage,{'songObj': songObj} );
    editSongModal.onDidDismiss(() => {
      this.ionViewDidEnter();
    });
    editSongModal.present();
  }

}
