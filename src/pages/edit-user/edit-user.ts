import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ControlProvider } from '../../providers/control/control';


@IonicPage()
@Component({
  selector: 'page-edit-user',
  templateUrl: 'edit-user.html',
})
export class EditUserPage {

  public user = {userId:'', firstName:'', lastName:'', userName:'', teamId:'', roleId: '', team:'', role:'', esAdmin:0, roleUserId: '', teamUserId: '', password:'', emailAdress:''}
  public editUserForm: FormGroup;
  public equipos = [];
  public roles = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, private cd: ChangeDetectorRef, private controlprovider: ControlProvider, public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
    this.user = this.navParams.get('user');
    this.user.password = '';
    this.createUserForm();
  }

  ionViewDidLoad() {
    this.getTeams();
  }

  public createUserForm(){
    //console.log(this.user);
    let esAdmin = this.user.esAdmin == 1 ? true : false;
    this.editUserForm = this.formBuilder.group({
      userName: [this.user.firstName, Validators.required],
      userLastName: [this.user.lastName, Validators.required],
      user: [this.user.userName, Validators.required],
      password: ['', Validators.minLength(6)], 
      equipos: [this.user.teamId, Validators.required],
      roles: [this.user.roleId, Validators.required],
      esAdmin: [esAdmin, Validators.required],
    });
  }

  editUser(){
    this.user.firstName = this.editUserForm.value.userName;
    this.user.lastName = this.editUserForm.value.userLastName;
    this.user.userName = this.editUserForm.value.user;
    this.user.teamId = this.editUserForm.value.equipos;
    this.user.roleId = this.editUserForm.value.roles;
    this.user.esAdmin = this.editUserForm.value.esAdmin;
    this.user.password = this.editUserForm.value.password;

    if(this.editUserForm.value.password != ''){
      this.updateUserWithPassword();
    }else{
      this.updateUser();
    }
  }

  updateUserWithPassword(){
    let loadinguuwp = this.loadingCtrl.create({
      content: 'Modificando usuario...'
    });
    loadinguuwp.present();
    this.controlprovider.setUserWithPassword(this.user).then(response =>{ 
      let data = JSON.parse(response.data);
      loadinguuwp.dismiss();
      this.updateTeamAndRole();
    },
    (error) =>{
      loadinguuwp.dismiss();
      console.log('Error updateUserWP-> '+JSON.stringify(error));
    });
  }

  updateUser(){
    let loadinguuwp = this.loadingCtrl.create({
      content: 'Modificando usuario...'
    });
    loadinguuwp.present();
    this.controlprovider.setUser(this.user).then(response =>{ 
      let data = JSON.parse(response.data);
      loadinguuwp.dismiss();
      this.updateTeamAndRole();
    },
    (error) =>{
      loadinguuwp.dismiss();
      console.log('Error updateUser-> '+JSON.stringify(error));
    });
  }
  
  updateTeamAndRole(){
    //console.log(this.user);
    
    let loadingutr = this.loadingCtrl.create({
      content: 'Modificando Equipo y Rol...'
    });
    loadingutr.present();
    this.controlprovider.setTeamAndRole(this.user).then(response =>{ 
      //console.log(response);
      loadingutr.dismiss();
      this.showSaved();
    },
    (error) =>{
      console.log('Error updateteamAndRole-> '+JSON.stringify(error));
    });
  }

  showSaved(){    
    const confirm = this.alertCtrl.create({
      title: '¡Modificado!',
      message: 'El usuario ha sido modificado exitosamente.',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    confirm.present();
  }

  getTeams(){
    this.controlprovider.getTeams().then(response =>{ 
      let data = JSON.parse(response.data);
      this.equipos = data['list'];
      this.editUserForm.value.equipos = this.user.teamId;
      this.getRoles();
    },
    (error) =>{
      console.log('Error getTeams-> '+JSON.stringify(error));
    });
  } 

  getRoles(){
    this.controlprovider.getRoles().then(response =>{ 
      let data = JSON.parse(response.data);
      this.roles = data['list'];
      this.editUserForm.value.roles = this.user.roleId;
      this.cd.detectChanges();
    },
    (error) =>{
      console.log('Error getRoles-> '+JSON.stringify(error));
    });
  }

}
