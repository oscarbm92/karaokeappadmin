import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, MenuController } from 'ionic-angular';
import { MesasPage } from '../mesas/mesas';
import { PlaylistPage } from '../playlist/playlist';
import { SomethingPage } from '../something/something';
import { UserAdminPage } from '../user-admin/user-admin';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  mesasRoot = MesasPage
  playlistRoot = PlaylistPage
  adminUserRoot = UserAdminPage
  somethingRoot = SomethingPage
  public user = {id:'', esAdmin:false};


  constructor(public navCtrl: NavController, public menuCtrl:MenuController, private cd: ChangeDetectorRef, private storage: Storage) {
    this.menuCtrl.swipeEnable(true);
    this.storage.get('user').then((val) => {
      this.user = val;
    });
  }

  onChange(){
    this.cd.detectChanges();
  }
}
