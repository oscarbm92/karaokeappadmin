import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import {MenuController, NavController, AlertController, LoadingController} from 'ionic-angular';
import { HomePage } from '../home/home';
import { Events } from 'ionic-angular';
import { ControlProvider } from '../../providers/control/control';
import firebase  from 'firebase';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { Firebase } from '@ionic-native/firebase';
import jwt from 'jwt-simple';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {

  public loginForm: FormGroup;
  public loginFormVisible:boolean = false;
  public welcomeVisible:boolean = false;
  public isFirstIn:boolean = true;
  public epochlogin = 0;
  public userData = null;

  constructor(public menuCtrl:MenuController, private storage: Storage, public navCtrl: NavController, 
    public events: Events, private controlprovider: ControlProvider, private formBuilder: FormBuilder,  
    public alertCtrl: AlertController, private cd: ChangeDetectorRef, public loadingCtrl: LoadingController, public firebasecm: Firebase) {
    this.menuCtrl.swipeEnable(false);
  }

  ionViewDidEnter() {  
    this.checkUserStatus();  
  }

  private createLoginForm(){
    this.loginForm = this.formBuilder.group({
      usernamelogin: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  checkUserStatus(){
    let loginLoad = this.loadingCtrl.create({
      content: 'Verificando cuenta...'
    });
    loginLoad.present();
    let t = this;
    firebase.auth().onAuthStateChanged(function(user) {
      loginLoad.dismiss();
      if (user) {
        t.userData = user
        t.welcomeVisible = true;
        t.cd.detectChanges();
      }else{
        t.createLoginForm();
        t.loginFormVisible = true;
        t.cd.detectChanges();
      }
    });
  }

  checkUserCrm(){    
    let logincheckUserCrm = this.loadingCtrl.create({
      content: 'Iniciando sesión...'
    });
    logincheckUserCrm.present();
    let t = this;
    this.controlprovider.getUserIsActive(this.userData.uid).then((response) => {
      let data = JSON.parse(response.data);
      if(data){
        t.storage.set('user', data);
        if(data.isActive){
          t.checkAndSetFirebaseToken(data);
          //setTimeout(() => {logincheckUserCrm.dismiss();t.navCtrl.setRoot(HomePage);}, 1000);
          logincheckUserCrm.dismiss();t.navCtrl.setRoot(HomePage);
        }else{
          t.showUserInactive();
          logincheckUserCrm.dismiss();
        }
      }
    },
    (error) =>{
      logincheckUserCrm.dismiss();
      console.log(error);
    });
  }

  checkAndSetFirebaseToken(data){
    let t = this;
    t.firebasecm.getToken().then(token => {
      t.controlprovider.setUserFirebaseToken(data.id, token).then((responsetk) => { 
      },
      (error) =>{
        console.log(error);
      });
    }) // save the token server-side and use it to push notifications to this device
    .catch(error => console.error('Error getting token', error));

    t.firebasecm.onTokenRefresh().subscribe((token: string) => {
      t.controlprovider.setUserFirebaseToken(data.id, token).then((responsetk) => { 
      },
      (error) =>{
        console.log(error);
        if(error.error == 'The host could not be resolved'){
          alert('¡Necesitas estas conectado a internet!');
        }
        
      });
    });
  }


  login(){
    this.epochlogin = (new Date).getTime();
    let loginFull = this.loadingCtrl.create({
      content: 'Iniciando sesión...'
    });
    loginFull.present();
    
    var username = this.loginForm.value.usernamelogin;
    var password = this.loginForm.value.password;
    var token = 'Basic ' + btoa(username + ":" + password);

    this.controlprovider.login(token, username).then((response) => {
      let data = JSON.parse(response.data);
      if(response.status === 401){
        loginFull.dismiss();
        this.showLoginError();
      }else{
        if(data['total'] == 0){
          loginFull.dismiss();
          this.showLoginError();
        }else{
          this.storage.set('user', data['list'][0]);
          this.checkAndSetFirebaseToken(data['list'][0]);
          if(data['list'][0].isActive){
           this.getToken(data['list'][0]).then((tkn) => {
              firebase.auth().signInWithCustomToken(tkn).then((user) => {
                firebase.auth().currentUser.updateEmail(data['list'][0].emailAddress);
                firebase.auth().currentUser.updateProfile({displayName: data['list'][0].name, photoURL: ''});
                if(user.additionalUserInfo.isNewUser){
                  loginFull.dismiss();
                  this.activateAccountCRM(user.user.uid);
                }else{
                  //setTimeout(() => {loginFull.dismiss(); this.navCtrl.setRoot(HomePage);}, 1000);
                  loginFull.dismiss(); this.navCtrl.setRoot(HomePage);
                }
              }).catch(function(error) {
                // Handle Errors here.
                loginFull.dismiss();
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(errorCode + ' - ' +errorMessage);
                // ...
              });
            })
          }else{
            this.showUserInactive();
          }
        }
      }
    },
    (error) =>{
      if(error['status'] === 401){
        loginFull.dismiss();
        this.showLoginError();
      }
      //console.log(error);
    });
  }

  async getToken(user){
    let time = (new Date).getTime()/1000;
    let payload = {alg:'RS256', 
    iss: 'firebase-adminsdk-l35xm@karaokeadmin.iam.gserviceaccount.com',
    sub: 'firebase-adminsdk-l35xm@karaokeadmin.iam.gserviceaccount.com',
    aud: 'https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit',
    iat: time,
    exp: time+3600,
    uid: user.id
    }

    return jwt.encode(payload, '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDfZPlVtya2a6q4\nzzYo71k88jY8ePnINHTFqSmikk29vc01bOQGHKY8Bsvb6xyvcJZ34wijTSmMEyY2\nPTwzl/h52D65M9jCQCmpAZePmBGWiz7l91hpjMGmUsC2H+DpZWEcKDQxs0scWt51\n80dTSZrAZCta1yiv8eyKBWADhXaYQROVKPwCto93e2x5ooncR/hIVWOWHl26oGYF\n9z+evKh89aLM9L2aKO9W87o9iFDNMCB4UMjhwiDvNXggjdpgaxIIpShRV1F60xh4\nF/hLHACAY7/CmQIduXwAG7zvkrLTv8QfXCo/8G0/4QpWWRqabM3Ylxrsw9aT0dQF\nM2zMOK2XAgMBAAECggEAA7v03QVwPLllA6zXC20g0T99P5qIFlwmHY271+9ZJFRR\nY1S8JQwCjVtRug3ej2Iy6bMeWFh4WtKRH3P7lyMxsJu4RLFErM6Uw7H2nhxdIuaV\n+RMe47jKHeUhOooi4NPkgSt8393O4hz0vgUAirhewVuLS/vvvq0tFouLdNOczWO1\nFaRX18J4jdFCxV0SGz/G/eInRFd34jD/TTohpx+9teoLFIpSH0Gd7PdPfynngxHq\n+QGJlAUEpyLaxV0aJIRlA03DCdjoAdnT3ZA0gJxtpu/k9N8YALPNpU36HAcDeLpL\n3FjMPcVG9FeFIRivCbwfef8RMj4Fb9ajS2sOcFeHlQKBgQD89cyFmON8qbYrT22U\n7b3S0Io070EUd+DHRRWEtz3TM1oik7ZK4x175CcGGwHfU1NFO2DP784RDYEHw1vG\nTokzLRqBVu4jshUQtIsG5/TMTd+c3qxO96M5Vu2Xc+BFpaBImZKShpt6A72Vlaip\n3v7AEZds5pZoHdpS+r6Vy+NKDQKBgQDiFDg+xdKIZVt+AqJxwsBN+XQ3iG4750oU\nPmKibLg9tSjDgT3TcR4aZU7Bz7UjjHT5ZqXCIhHQ/Bv7eK7dIFyp/7LEC9VKlXzI\nOj7CsZHvhqGa8P/GPdnjhszWYWD89LDt/nKkU6zhGL3Pgux3LW4BIWyH+nFGTNIz\nU/wPG+phMwKBgQCH518MX8nIjj1rI29SVjBn9NZr/ibO4mEHblxTQ0eaErrYXNfR\ngBe1GizFIPlujIvuZCptTP8/NiweiZzi/ArGTfMxjgy/bMvYf8c53KEuqp/63b9h\nZbZx9fwMJJtXj9Z/atLOmjcp5JyUoyVMrpvbgRgD0Bb6tR84+bIvNK1WeQKBgHQp\nw4StjLG8srgajnE5iioGhOqL1Wg+PVgNfgzLJBfA38MkwynDs54Ic+Et4nrzjp8f\nWQhZDSJ+h7o8lkzUdgh7c9U750o9aTJQizLqnnueh6bfMe0glrZL6zA0NOuEpuyC\nk1FQFUJLvivDr9DErQMzs1pjGpTuoArzmk4BH/r1AoGBAPOJzZkKg3pQY5W3ubzH\nynPShjNB2Gv9BV/5jt3DrCQ8OufFQ9CcRzf0OvyZau6h2HfsQPaVyyEoE7qEOAP+\nXE+DFyuYTd88GxvxnhWzFy+zpdC+nNveGcGkuxPOLsslsr1MqGhTiRFG89H7W02N\nczVyiXPVILDHaYv8USor6/uR\n-----END PRIVATE KEY-----\n', 'RS256');
  }

  activateAccountCRM(idUser){
    let loadingCRM = this.loadingCtrl.create({
      content: 'Configurando cuenta...'
    });
    loadingCRM.present();
    this.controlprovider.setCuentaAppCreada(idUser).then((response) => {
      //setTimeout(() => {loadingCRM.dismiss(); this.navCtrl.setRoot(HomePage);}, 1000);
      loadingCRM.dismiss(); this.navCtrl.setRoot(HomePage);
      
    },
    (error) =>{
      console.log(error);
    });
  }

  ////////////////////////// ALERTAS //////////////////////////////////////////////////

  showNoUser(){    
    const confirm = this.alertCtrl.create({
      title: 'No ingresó ningun usuario',
      message: 'Por favor introduzca un usuario para poder recuperar su contraseña.',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  showLoginError(){    
    const confirm = this.alertCtrl.create({
      title: 'Error',
      message: 'Usuario y/o contraseña incorrecta, intentelo nuevamente.',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    confirm.present();
  }

  showUserInactive(){    
    const confirm = this.alertCtrl.create({
      title: 'UPS!',
      message: 'Parece que tu usuario esta inactivo.',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            firebase.auth().signOut()
          }
        }
      ]
    });
    confirm.present();
  }

}
