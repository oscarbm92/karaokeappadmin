import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Searchbar, AlertController, LoadingController, ViewController, App } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { YoutubeProvider } from '../../providers/youtube/youtube';
import { KaraokeProvider } from '../../providers/karaoke/karaoke';
import { Storage } from '@ionic/storage';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

@IonicPage()
@Component({
  selector: 'page-edit-song',
  templateUrl: 'edit-song.html',
})
export class EditSongPage {

  @ViewChild('ytsb') searchbar: Searchbar;

  searchResult: Observable<any[]>;
  public bgbuscar: boolean = true;
  public loading: boolean = false;
  public sinresultados: boolean = false;
  public isEdit: boolean = true;
  public loadingAddSong = this.loadingCtrl.create({
    content: 'Editando Canción...'
  });
  public song = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, 
    private ytprovider: YoutubeProvider, public storage: Storage, private karaokeprovider: KaraokeProvider,
    public alertCtrl: AlertController, public loadingCtrl: LoadingController, private cd: ChangeDetectorRef, private app: App, private youtube: YoutubeVideoPlayer) {

    console.log(navParams.get('songObj'));
    this.song = navParams.get('songObj');
  }

  seacrhSong(q) {    
    let loadingsearching = this.loadingCtrl.create({
      content: 'Buscando canciones...'
    });
    loadingsearching.present();

    this.ytprovider.getYoutubeSearch(q).then(response =>{
      let data = JSON.parse(response.data);
      console.log(data);
      this.searchResult = data['items'];
      this.sinresultados = data['items'].length>0 ? false : true;
      this.bgbuscar = false;
      this.loading = false;
      loadingsearching.dismiss();
      this.cd.detectChanges();
      
    },
    (error) =>{
      console.log('Error getYoutubeSearch -> '+JSON.stringify(error));
    });  
  }

  onInput(q){    
    if(q){
      if(q.target.value == '' || q.target.value == 'undefined' || q.target.value == null){
        this.bgbuscar = true;
        this.sinresultados = false;
        this.cd.detectChanges();
        this.searchResult = null;
      }else{
        this.loading = true;
        this.cd.detectChanges();
        this.seacrhSong(q);
      }
    }
  }

  onCancel(){

  }

  showConfirmAdd(songObj){
    console.log(this.song);
    
    const confirm = this.alertCtrl.create({
      title: 'Editar Canción',
      message: `
      <p>Seguro/a que quieres cambiar:</p>
      <ul>
        <li>`+this.song.name+`</li>
      </ul>
      <p>Por:</p>
      <ul>
        <li>`+songObj.snippet.title+`</li>
      </ul>
    `,
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Si',
          handler: () => {
            songObj.calificable = true;
            this.editSong(songObj, this.song);
          }
        }
      ]
    });
    confirm.present();
  }
  
  editSong(newSong, oldSong) {


    let loadingAddSong = this.loadingCtrl.create({
      content: 'Editando Canción...'
    });
    loadingAddSong.present();
    this.karaokeprovider.editSong(newSong, oldSong).then(response =>{ 
      console.log(response);
      this.karaokeprovider.orderPlaylist().then(response =>{ 
        console.log(response);
      },
      (error) =>{
        console.log('Error orderPlaylist-> '+JSON.stringify(error));
      });
      
      loadingAddSong.dismiss();
      this.searchResult = null;
      this.searchbar.clearInput(null);
      this.bgbuscar = true;
      this.cd.detectChanges();
      this.showSongEdited();
    },
    (error) =>{
      console.log('Error addtoplaylist-> '+JSON.stringify(error));
    });
  }

  showSongEdited(){
    const confirm = this.alertCtrl.create({
      title: '¡Es todo!',
      message: 'Tu canción ha sido modificada.',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.viewCtrl.dismiss();
            this.song = null;
            //this.navCtrl.pop();
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewDidLoad() {
    let elem = <HTMLInputElement>document.querySelector('#ytsearchbar input');
    if (elem) {
        elem.focus();
    }
  }

  ionViewDidEnter() {
    let elem = <HTMLInputElement>document.querySelector('#ytsearchbar input');
    if (elem) {
        elem.focus();
    }
  }

  public onClickCancel() {
    this.viewCtrl.dismiss();
  }

  previewVideo(idVideo){
    this.youtube.openVideo(idVideo);
  }

}
