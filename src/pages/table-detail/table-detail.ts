import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ControlProvider } from '../../providers/control/control';
/**
 * Generated class for the TableDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-table-detail',
  templateUrl: 'table-detail.html',
})
export class TableDetailPage {

  public newTableForm: FormGroup;
  public mesa = {description: '', songsLimit: 0};

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, private controlprovider: ControlProvider, public alertCtrl: AlertController) {
    this.mesa = this.navParams.get('mesa');
    console.log(this.mesa);
    this.createNewTableForm();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TableDetailPage');
  }

  private createNewTableForm(){
    this.newTableForm = this.formBuilder.group({
      maximoCanciones: [this.mesa.songsLimit, Validators.required],
      description: [this.mesa.description]
    });
  }

  saveUser(){
    this.mesa.description = this.newTableForm.value.description;
    this.mesa.songsLimit = this.newTableForm.value.maximoCanciones;

    this.controlprovider.editMesa(this.mesa).then((responseNM) => {
      this.showSaved();
    },
    (error) =>{
      console.log('Error editMesa-> '+JSON.stringify(error));
    });
  }

  showSaved(){    
    const confirm = this.alertCtrl.create({
      title: '¡Modificado!',
      message: 'La mesa ha sido modificada exitosamente.',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });
    confirm.present();
  }

  

}
