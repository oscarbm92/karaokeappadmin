import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, AlertController, LoadingController, App, ActionSheetController } from 'ionic-angular';
import { ControlProvider } from '../../providers/control/control';
import { Storage } from '@ionic/storage';
import { NewTablePage } from '../new-table/new-table';
import QRCode from 'qrcode';
import firebase  from 'firebase';
import { LoginPage } from '../login/login';
import { TableDetailPage } from '../table-detail/table-detail';


@Component({
  selector: 'page-mesas',
  templateUrl: 'mesas.html',
})
export class MesasPage {

  public mesas = null;
  public user = '';
  public filtroMesas = 'todo';
  public userData = {esAdmin:false};
  public isFirstIn = true;

  constructor(public navCtrl: NavController, private controlprovider: ControlProvider, private storage: Storage, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController, private cd: ChangeDetectorRef, private app: App, private actionSheetCtrl:  ActionSheetController) {
    
    this.storage.get('user').then((val) => {
      //console.log(val);
      this.userData = val;
      this.user = val.id;
    });    
  }

  ionViewDidEnter() {
    console.log('mesas didEnter');
    
    this.getMesas(); 
  }

  openNewTable(){
    let statusloading = this.loadingCtrl.create({
      content: 'Revisando Status...'
    });
    statusloading.present();
    let t = this;
    
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        t.controlprovider.getUserIsActive(user.uid).then((response) => {
          let data = JSON.parse(response.data);
          if(data){
            if(data.isActive){
              statusloading.dismiss();
              t.navCtrl.push(NewTablePage);
            }else{
              statusloading.dismiss();
              t.showUserInactive();
            }
          }
        },
        (error) =>{
          statusloading.dismiss();
          console.log(error);
        });
      }else{
        t.showUserInactive();
        statusloading.dismiss();
      }
    });
  }

  editTable(mesa){
    let statusloading = this.loadingCtrl.create({
      content: 'Revisando Status...'
    });
    statusloading.present();
    let t = this;
    
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        t.controlprovider.getUserIsActive(user.uid).then((response) => {
          let data = JSON.parse(response.data);
          if(data){
            if(data.isActive){
              statusloading.dismiss();
              t.navCtrl.push(TableDetailPage, {mesa:mesa});
            }else{
              statusloading.dismiss();
              t.showUserInactive();
            }
          }
        },
        (error) =>{
          statusloading.dismiss();
          console.log(error);
        });
      }else{
        t.showUserInactive();
        statusloading.dismiss();
      }
    });
  }

  showUserInactive(){    
    const confirm = this.alertCtrl.create({
      title: 'UPS!',
      message: 'Parece que tu usuario esta inactivo.',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            firebase.auth().signOut();
            this.app.getRootNav().setRoot(LoginPage);
          }
        }
      ]
    });
    confirm.present();
  }

  getMesas(){
    let getMesasLoading = this.loadingCtrl.create({
      content: 'Cargando mesas...'
    });
    getMesasLoading.present();
    this.controlprovider.getMesas().then((response) => { 
      let data = JSON.parse(response.data);
      //console.log(data['list'])
      this.mesas = data['list'];
      getMesasLoading.dismiss();
      this.onChangeFilter(this.filtroMesas);
    },
    (error) =>{
      console.log('Error getMesas-> '+JSON.stringify(error));
      getMesasLoading.dismiss();
    });
  }

  doRefresh(refresher) {
    this.controlprovider.getMesas().then((response) => { 
      let data = JSON.parse(response.data);
      //console.log(data['list'])
      this.mesas = data['list'];
      this.onChangeFilter(this.filtroMesas);
      refresher.complete();
    },
    (error) =>{
      console.log('Error getMesas-> '+JSON.stringify(error));
    }); 
  }

  generateQr(idMesa, index){
    const qrcode = QRCode;
    let t = this;
    qrcode.toDataURL(idMesa, { errorCorrectionLevel: 'H' }, function (err, url) {
      t.mesas[index].qrimg = url;
    })
    this.cd.detectChanges();
  }

  cerrarMesa(idMesa){
    let loadingCerrarMesa = this.loadingCtrl.create({
      content: 'Cerrando Mesa...'
    });
    loadingCerrarMesa.present();
    let t = this;
    t.controlprovider.getClientsByTable(idMesa).then((response) => { 
      let data = JSON.parse(response.data);
      Object.keys(data['list']).forEach(function(key,index) {
        t.controlprovider.desactivateUser(data['list'][key].id).then((response) => {
        },
        (error) =>{
          console.log('Error desactivateUser-> '+JSON.stringify(error));
        }); 
        t.controlprovider.deleteVotes(data['list'][key].id).then((response) => {
        },
        (error) =>{
          console.log('Error deleteVotes-> '+JSON.stringify(error));
        }); 
      });


      t.controlprovider.getSongsByTable(idMesa).then((response) => { 
        let data = JSON.parse(response.data);
        Object.keys(data['list']).forEach(function(key,index) {
          //if(!data['list'][key].played){
            t.controlprovider.deleteSong(data['list'][key].id).then((response) => {
            },
            (error) =>{
              console.log('Error deleteSong-> '+JSON.stringify(error));
            }); 
          //}
        });


        t.controlprovider.desactivateTable(idMesa).then((response) => {
          loadingCerrarMesa.dismiss();
          this.getMesas();
        },
        (error) =>{
          console.log('Error desactivateTable-> '+JSON.stringify(error));
        }); 


      },
      (error) =>{
        console.log('Error getSongsByTable-> '+JSON.stringify(error));
      }); 


    },
    (error) =>{
      console.log('Error getClientsByTable-> '+JSON.stringify(error));
    }); 
  }

  showConfirmCloseTable(mesa){
    console.log(mesa);
    
    const confirm = this.alertCtrl.create({
      title: 'CERRAR MESA '+mesa.name,
      message: '¿Desea cerrar la mesa '+ mesa.name +'? Se eliminarán todas sus canciones en cola y sus usuarios ya no podrán interactuar con el sistema.',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
        },
        {
          text: 'Si',
          handler: () => {
            this.cerrarMesa(mesa.id);
          }
        }
      ]
    });
    confirm.present();
  }

  /*activateUser(){
    this.controlprovider.activateUser(this.userData.id).subscribe(data =>{ 
        this.navCtrl.push(KaraokePage);
    },
    (error) =>{
      console.log('Error getuserKaraoke-> '+JSON.stringify(error));
    });
  }*/

  onChangeFilter(value){
    let t = this;
    if(value == 'misMesas'){
      Object.keys(this.mesas).forEach(function(key,index) {
        if(t.user == t.mesas[index].assignedUserId){
          t.mesas[index].show = true;
          
        }else{
          t.mesas[index].show = false;
        }
      });
    }else{
      Object.keys(this.mesas).forEach(function(key,index) {
        t.mesas[index].show = true;
      });
    }
    if(!t.isFirstIn){
      t.cd.detectChanges();
    }else{
      t.isFirstIn = false;
    }
  }

}
