import { Component, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { ControlProvider } from '../../providers/control/control';
import { EditUserPage } from '../edit-user/edit-user';
import { NewUserPage } from '../new-user/new-user';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-user-admin',
  templateUrl: 'user-admin.html',
})
export class UserAdminPage {

  public users = null;
  public userData = {};
  

  constructor(public navCtrl: NavController, public navParams: NavParams, private controlprovider: ControlProvider, private cd: ChangeDetectorRef, 
    public loadingCtrl: LoadingController, public alertCtrl: AlertController, private storage: Storage) {

      this.storage.get('user').then((val) => {
        this.userData = val;
      });
  }
  

  /*ionViewDidLoad() {
    this.getUsers();
  }*/

  ionViewDidEnter(){
    this.getUsers();
  }

  openNewUser(){
    this.navCtrl.push(NewUserPage);
  }

  getUsers(){
    let loadingUsers = this.loadingCtrl.create({
      content: 'Cargando usuarios...'
    });
    loadingUsers.present();
    this.controlprovider.getUsers().then(response =>{ 
      let data = JSON.parse(response.data);
      this.users = data;
      console.log(this.users);    
      loadingUsers.dismiss();
      this.cd.detectChanges();
    },
    (error) =>{
      console.log('Error getUsers-> '+JSON.stringify(error));
    });
  }

  doRefresh(refresher) {
    let loadingUsers = this.loadingCtrl.create({
      content: 'Cargando usuarios...'
    });
    loadingUsers.present();
    this.controlprovider.getUsers().then(response =>{ 
      let data = JSON.parse(response.data);
      this.users = data;
      console.log(this.users);
      refresher.complete();
      loadingUsers.dismiss();
      this.cd.detectChanges();
    },
    (error) =>{
      console.log('Error getUsers-> '+JSON.stringify(error));
    });
  }

  changeUserStatus(id, status){
    let loadingChangeUserStatus = this.loadingCtrl.create({
      content: 'Cambiando status...'
    });
    loadingChangeUserStatus.present();

    let newStatus = status == 1 ? false : true;

    this.controlprovider.changeUserStatus(id, newStatus).then(response =>{ 
      let data = JSON.parse(response.data);
      loadingChangeUserStatus.dismiss();
      this.ionViewDidEnter();
      
    },
    (error) =>{
      console.log('Error changeUserStatus-> '+JSON.stringify(error));
    });
  }

  editUser(user){
    this.navCtrl.push(EditUserPage, {user:user});
  }

  deleteUser(id){
    let loadingDelete = this.loadingCtrl.create({
      content: 'Eliminando Usuario...'
    });
    loadingDelete.present();
    this.controlprovider.deleteUser(id).then(response =>{ 
      let data = JSON.parse(response.data);
      loadingDelete.dismiss();
      this.ionViewDidEnter();
    },
    (error) =>{
      console.log('Error getUsers-> '+JSON.stringify(error));
      loadingDelete.dismiss();
    });
  }

  showDeleteUser(user){    
    const confirm = this.alertCtrl.create({
      title: 'Eliminar usuario',
      message: '¿Segur/a que quiere eliminar al usuario: '+ user.userName,
      buttons: [{
        text: 'NO',
        handler: () => {
          
        }
      },
      {
        text: 'SI',
        handler: () => {
          this.deleteUser(user.userId);
        }
      }
      ]
    });
    confirm.present();
  }

}
